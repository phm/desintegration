from random import randint
import matplotlib.pyplot as plt
import time
import numpy as np

def LancéDés(nbr):
    """
    Entrée : nombre de dés
    Sortie : nombre de dés restants après un lancé
    """
    
    # S'assurer que le nombre de dés entrés est un entier 
    if int(nbr) != nbr :
        return "Entrer un nombre entier de dés"
         
    dés_courants = nbr
    
    for k in range (0,nbr): #La boucle s'effectue sur le nombre de dés
         
         if randint(1,6) == 1 : #randint renvoie un entier aléatoire compris entre 1 et 6 inclus
             
             dés_courants = dés_courants  - 1 # Si la valeure tirée est de 1, le dé n'est pas conservé 
             

    return dés_courants # On renvoie le nombre de dés conservés
    
def LancésDés(nbr,ité) :
    """
    Entrées : nombre de dés, nombre de lancés (nombre d'itérations)
    Sortie : nombre de dés restants après tous les lancés
    """
    dés_courants = nbr # On démarre les lancés avec l'ensemble des dés
    
    for k in range (0,ité): # La boucle s'effectue sur le nombre de lancés
        
        a = LancéDés(dés_courants) # Entrer dans la variable a le résultat de la fonction LancéDés précédemment définie
        dés_courants = a 
        
    return dés_courants # On renvoie le nombre de dés restants après le nombre total de lancés
    
    
    
def Données_finales(nbr) :
    """
    Entrées : nombre de dés
    Sortie : liste du nombre de dés après chaque lancé
    """
    
    Liste =[] # Création d'une liste vide
    dés_courants = nbr 
    
    
    while dés_courants != 0: # Tant que le nombre "dés_courants" n'est pas nul, on effectue la boucle
        
        Liste.append(dés_courants) # On ajoute à la liste la valeure de dés_courants
        a = LancéDés(dés_courants) 
        dés_courants = a 
        
    return Liste # On renvoie la liste
    
    
def Tracé(nbr):
    
    Y = Données_finales(nbr) 
    X = [k for k in range (0,len(Y))] # En abscisse, on a les entiers de 0 au nombre d'éléments de la liste moins un
    
    
    x = [k for k in range (0,len(Y))]
    z = [-0.18 * k for k in range (0,len(x))]
    y = np.exp(z) * nbr
    
    
# On trace Y en fonction de X    
    plt.plot(X,Y,linestyle='',marker='o')
    plt.plot(x,y)
    plt.grid()
    plt.show()
    
    


def Temps(nbr) :
     
    t1 = time.time()
     
    Liste =[] 
    dés_courants = nbr 
    
    
    while dés_courants != 0: 
        
        Liste.append(dés_courants) 
        a = LancéDés(dés_courants) 
        dés_courants = a 
     
     
    t2 = time.time()
    
    return t2 - t1

    
def TracéT (i):
    z = [(10**k) for k in range (2 , i)]
    x = [np.log((10**k)) for k in range (2 , i)]
    y = [Temps(k) for k in z]
    
    plt.plot(x,y)
    plt.grid()
    plt.show()
    