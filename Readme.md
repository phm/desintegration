# Simulation Python de la simulation de la désintégration radioactive par lancers de dés.

article de Marie-Thérèse Lehoucq

## Terminologie

un dé = un atome

un ensemble de dés = une collection d'atomes


## Organisation

* `de.py` source Python du code de simulation
* `desintegration.md` source Markdon/Pandoc de l'article

* `Readme.md`, `Makefile`... as usual 

* `import/` version initiale Marie-Thérèse
