DOC := desintegration

A4PAPER = -V "geometry:a4paper,includeheadfoot,margin=2.54cm"
NSECTIONS = --number-sections -V "secnumdepth:1"

_ : all
	open desintegration.pdf

all : $(addsuffix .pdf, ${DOC})

# paper
%.pdf : %.md
	pandoc -s $(A4PAPER) $^ -o $@

# clean and cie
clean:
	rm -f $(addsuffix .pdf, ${DOC}) $(addsuffix -v.pdf, ${DOC})
realclean : clean
	rm -f *~ 
.PHONY: _ all clean realclean



