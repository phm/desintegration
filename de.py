#! /usr/bin/python3
# -*- coding: utf-8 -*-

"""
:mod:`desintegration` module

:author: 

:date: juillet 2018

Simulation Python de la simulation de la désintégration radioactive par lancers de dés.

"""

from random import randint

def generation_suivante(ndes) :
    """
    Simulation d'un pas de temps. 
    Paramètre: nombre de dés modélisant une génération. 
    Valeur renvoyée: nombre de dés restants à la génération suivante.
    """
    d_restants = ndes
    for _ in range(0, ndes) :             # pour chaque dé
        lancer = randint(1, 6)            # la valeur du dé, entre 1 et 6
        if lancer == 1 :                  # si 1, désintégration
            d_restants = d_restants - 1   # le dé est supprimé
    return d_restants

def les_generations(ndes) :
    """
    Simulation à partir d'une population initiale d'atomes / dés.
    Termine quand il n'y a plus de dés.
    Paramètre: nombre de dés modélisant la population initiale.
    Valeur renvoyée: liste des nombres de dés de chaque génération. 
    """
    l = []                                # liste initialement vide
    while ndes !=0 :                      # il reste des dés
        l.append(ndes)                    # ajoute un élément à la liste
        ndes = generation_suivante(ndes)  # nombre de dés restants
                                          # après un nouveau lancer
    return l

import matplotlib.pyplot as plt           # fonctions de tracés de courbes
from math import exp                      # fonction exponetielle

def trace_courbe(populations) :
    """
    Trace la courbe de l'évolution d'une collection d'atomes donnée. 
    Paramètre: la liste des nombres de dés/atomes de chaque génération.
    Valeur renvoyée: aucune.
    """
    # population initiale
    n_atomes = populations[0]
    # nombre de générations    
    n_generations = len(populations)

    # abscisses, les pas de temps, de 0 à n_generations
    temps = list(range(0, n_generations))

    # decr_exp : décroissance exponentielle théorique
    decr_exp = [0] * n_generations        # une liste de n_generations éléments
    for t in range(0, n_generations) :
        # n_atomes * e^(-t/6) 
        decr_exp[t] = n_atomes * exp(-t/6)

    # tracé des points, nombre d'atomes/dés à chaque génération, populations
    # en fonction de temps
    plt.plot(temps, populations, linestyle='', marker='o')
    # tracé de la courbe théorique, decr_exp en fonction de temps
    plt.plot(temps, decr_exp)
    # tracé des axes
    plt.grid()
    # affichage 
    plt.show()

from time import process_time             # prise de temps, en secondes

def duration1 (f, x) :
    """
    Temps d'exécution d'un appel d'une fonction à un paramètre.
    Paramètres: la fonction et le paramètre
    Valeur renvoyée: le temps d'exécution d'un appel de la
        fonction. Le résultat de la focntion est ignoré.
    """
    start = process_time()
    f(x)
    duration = process_time() - start
    return duration
