---
title: Simuler la désintégration radioactive d’une collection d’atomes avec Python 3
author: |
  | Par Marie-Thérèse LEHOUCQ
  | Lycée Victor Duruy-75007 Paris
abstract : >
	Au lycée, en physique, programmer en Python 3 est ce possible ?
	Oui, en revisitant un modèle de dés de la désintégration
	radioactive d’une collection d’atomes. Et chemin faisant, la
	notion de complexité algorithmique est abordée.
lang: fr
---

Durée de la séquence
--------------------

Les élèves des classes de premières et ou de terminales scientifiques
travaillent pendant une heure et demi-heure.

Prérequis des élèves
--------------------

Éléments de base du langage Python : structure if, boucle for, boucle
while, définition de fonctions, listes. Utilisation du mode console de
Python.

Introduction
------------

Pour établir que la durée de vie d’un atome radioactif ne peut être
connue et que seule sa probabilité de désintégration par unité de
temps peut être accessible, l’idée est de recourir à un modèle.

Dans les années 2000, un tel modèle utilisé par les enseignants en
lycée et en série scientifique était constitué de quelques centaines
de dés cubiques. La simulation consistait à réaliser des lancers, sans
remises. En effet, on retirait à chaque lancer les dés tombés sur un
chiffre indiquant par convention que le dé s’était désintégré. La
probabilité d’obtenir ce chiffre est d’un sixième pour un dé
cubique. L’intervalle de temps se ramenait à la durée entre deux
lancés successifs. Tout en manipulant, les élèves visualisaient alors
la loi de décroissance radioactive grâce au nombre de dés jetés
à chaque lancé sans remise. Le caractère aléatoire d’une
désintégration radioactive s’imposait tout naturellement. De plus,
travailler avec un tel modèle était en synergie avec l’enseignement
des probabilités que devait assurer nos collègues de
mathématiques.

Aujourd’hui, il est possible de revisiter un tel modèle en utilisant
les connaissances et savoir-faire des élèves en terme de programmation
Python. Des environnements de programmation Python tels Pyzo ou Thonny
rendent l'utilisation de Python facilement accessible.

1. Le langage de programmation Python
=====================================

Python est un langage de programmation généraliste, très largement
répandu. Python est également largement utilisé dans le cadre de
l'enseignement : sa syntaxe est simple, facilitant la lecture et
l'écriture de programmes ; il possède quelques bonnes propriétés
facilitant son apprentissage et imposant de bonnes pratiques de
programmation.

Python dispose de plus d’une très large bibliothèque qu'il est
possible d'utiliser directement, par exemple pour tracer des courbes.

L'utilisation de Python est également facilitée par les divers
environnements de programmation qui fournissent les outils écrire des
programmes, les exécuter, ou les mettre au point. Citons les
environnements Pyzo ou Thonny.


2. Simulation et tracé de courbes
=================================

Il s’agit d’écrire le programme que permettra de remplacer le modèle
de dés décrit en introduction. Ce programme est divisé en quatre
parties. Chaque partie est commentée afin de permettre une bonne
compréhension des lignes de code la constituant.


Simulation d'un pas de temps
----------------------------

Simuler un pas de temps consiste à lancers autant de dés que d'atomes
à une génération considérée, et ne conserver que ceux pour lesquels ce
lancer ne produit pas une valeur choisie, par exemple 1.

Une simulation Python de cette étape consiste, pour chacun des dés
à obtenir une valeur aléatoire comprise entre 1 et 6 modélisant le
lancer du dé. Nous allons utiliser la fonction
`randint()`{.py} du module Python `random`{.py} : 

````{.py}
from random import randint
````

pour définir une fonction de simulation d'un pas de temps :

````{.py}
def generation_suivante(ndes) :
    """
    Simulation d'un pas de temps. 
    Paramètre: nombre de dés modélisant une génération. 
    Valeur renvoyée: nombre de dés restants à la génération suivante.
    """
    d_restants = ndes
    for _ in range(0, ndes) :             # pour chaque dé
        lancer = randint(1, 6)            # la valeur du dé, entre 1 et 6
        if lancer == 1 :                  # si 1, désintégration
            d_restants = d_restants - 1   # le dé est supprimé
    return d_restants
````

Suite à la définition de cette fonction `generation_suivante()`{.py},
quelques manipulations et tests peuvent être menés dans la console
Python :

````{.py}
>>> generation_suivante(100)
84
>>> generation_suivante(100)
85
>>> generation_suivante(100)
82
>>> generation_suivante(100)
74
>>> generation_suivante(100)
81
>>> generation_suivante(100)
85
>>> generation_suivante(1000)
852
>>> generation_suivante(10**6)
833414
>>> generation_suivante(10**6)
833731
>>>
````

On remarquera bien entendu la variabilité du résultat d'un appel
à l'autre de la fontion.


Simulation d'une série de pas de temps
--------------------------------------

------------------------------------------------------------
**todo** question :
------------------------------------------------------------

  Est-ce que l'on garde cet aspect qui était fournit par la
  fonction `LancersDés()`{.py} de l'article original ?

  * cette fonction n'était pas utilisée dans la suite de l'article
  * elle a cependant un intérêt en terme de programmation (boucle `while`{.py}).
  * voir qu'en dire en terme de physique ?

------------------------------------------------------------


Simulation d'une collection d'atomes
------------------------------------

La simulation de la désintégration d'une population d'atomes consiste
en des simulations successives d'un pas de temps, jusqu'à ce qu'aucun
atome ne reste.

Le résultat produit par cette simulation est le nombre d'atomes
restants à chaque pas de temps. En Python, nous allons représenter ce
résultat par une liste d'entiers qui seront les nombres d'atomes des
générations successives :


````{.py}
def les_generations(ndes) :
    """
    Simulation à partir d'une population initiale d'atomes / dés.
    Termine quand il n'y a plus de dés.
    Paramètre: nombre de dés modélisant la population initiale.
    Valeur renvoyée: liste des nombres de dés de chaque génération. 
    """
    l = []                                # liste initialement vide
    while ndes !=0 :                      # il reste des dés
        l.append(ndes)                    # ajoute un élément à la liste
        ndes = generation_suivante(ndes)  # nombre de dés restants
                                          # après un nouveau lancer
    return l
````

Des exécutions de cette fonction donnent par exemple :

````{.py}
>>> les_generations(100)
[100, 93, 78, 68, 58, 48, 43, 37, 29, 27, 23, 19, 16, 14, 14, 8, 6, 5,
 4, 3, 3, 3, 2, 2, 1, 1, 1, 1, 1]
>>> les_generations(100)
[100, 86, 70, 60, 54, 45, 38, 29, 22, 20, 13, 12, 11, 8, 7, 7, 6, 6,
 5, 5, 4, 3, 3, 3, 3, 2, 2, 2, 2, 2, 2, 1, 1]
>>> les_generations(100)
[100, 80, 67, 58, 50, 35, 31, 22, 19, 17, 14, 11, 7, 6, 3, 3, 3, 2, 2]
>>>
````

Tracé de l'évolution de la population
-------------------------------------

Le module `matplotlib`{.py} de Python, et en particulier la fonction
`plot()`{.py} permet de tracer une courbe ou un ensemble de points
à partir de listes de coordonnées des points. Différents
arguments optionnels de cette fonction spécifie le style du rendu de
la courbe. 

On indique habituellement utiliser ce module sous le nom `plt` :

````{.py}
import matplotlib.pyplot as plt
````

Une fonction peut être définie pour produire l'affichage de
l'évolution du nombre d'atomes :

````{.py}
from math import exp                      # fonction exponetielle

def trace_courbe(populations) :
    """
    Trace la courbe de l'évolution d'une collection d'atomes donnée. 
    Paramètre: la liste des nombres de dés/atomes de chaque génération.
    Valeur renvoyée: aucune.
    """
    # population initiale
    n_atomes = populations[0]
    # nombre de générations    
    n_generations = len(populations)

    # abscisses, les pas de temps, de 0 à n_generations
    temps = list(range(0, n_generations))

    # decr_exp : décroissance exponentielle théorique
    decr_exp = [0] * n_generations        # une liste de n_generations éléments
    for t in range(0, n_generations) :
        # n_atomes * e^(-t/6) 
        decr_exp[t] = n_atomes * exp(-t/6)

    # tracé des points, nombre d'atomes/dés à chaque génération, populations
    # en fonction de temps
    plt.plot(temps, populations, linestyle='', marker='o')
    # tracé de la courbe théorique, decr_exp en fonction de temps
    plt.plot(temps, decr_exp)
    # tracé des axes
    plt.grid()
    # affichage 
    plt.show()
````

Le tracé de courbe pour 100 dés nécessite 1- de simuler l'évolution
d'une population, et 2- demander le tracé de la courbe :

````{.py}
>>> liste = les_generations(100)
>>> trace_courbe(liste)
````

ou directement
````{.py}
>>> trace_courbe(les_generations(100))
````

On obtient des courbes telles celles des deux figures suivantes. Les
points bleus correspondent aux nombres de dés pour chaque pas de temps
(produits par l'appel `plt.plot(temps, populations, linestyle='',
marker='o')`{.py}, pas de ligne, un point pour chaque donnée, en bleu
par défaut), les lignes orange correspondent à un modèle de
décroissance exponentielle avec une probabilité de désintégration d’un
sixième (produit par l'appel `plt.plot(temps, decr_exp) `{.py}).

![Cent dés, une exécution](trace-100-1.png){#toto}

![Cent dés, une autre exécution](trace-100-2.png) 


De même, des tracés pour 240 dés (cas des « staticubes » de chez
Pierron), 1000 dés et 1 million de dés ont été produits.

![Deux cent quarante dés, une première exécution](trace-240-1.png) 

![Deux cent quarante dés, une seconde exécution](trace-240-1.png) 

![Mille dés, première exécution ](trace-1000-1.png) 

![Mille dés, seconde exécution](trace-1000-2.png) 

![Un million de dés](trace-10p6.png) 

À partir d’un million de dés, l’accord entre la simulation et
l’exponentielle est parfait. Pour 1000 dés, il est tout à fait
satisfaisant. En revanche pour 240 dés, cet écart est visible sur les
simulations présentées. 


3. Prolongements
================

Performance et complexité algorithmique
---------------------------------------

_Est-ce que le programme écrit va vite ?_ Oui, la simulation de la
désintégration par un million de dés prend environ 10 secondes sur un
ordinnateur standard.

On peut mesurer ce temps d'exécution à l'aide d'une fonction telle

````{.py}
from time import process_time             # prise de temps, en secondes

def duration1 (f, x) :
    """
    Temps d'exécution d'un appel d'une fonction à un paramètre.
    Paramètres: la fonction et le paramètre
    Valeur renvoyée: le temps d'exécution d'un appel de la
        fonction. Le résultat de la focntion est ignoré.
    """
    start = process_time()
    f(x)
    duration = process_time() - start
    return duration
````

Quelques exécutions dans le shell Python :

````{.py}
>>> duration1(les_generations, 100)
0.00259100000000001
>>> duration1(les_generations, 1000)
0.012942999999999927
>>> duration1(les_generations, 10000)
0.13356999999999998
>>> duration1(les_generations, 10**6)
13.322968
>>> duration1(les_generations, 10**7)
135.865997
````

On observe que le temps d'exécution semble proportionnel au nombre de
dés. Par exemple, le passage d’une simulation avec un million de dés
à une simulation avec dix millions de dés conduit à des durées
respectives de 13,3 secondes et de 135,8 secondes. 

----
**todo** Complexité
----

  _Est ce que le programme peut aller plus vite ?_ Non, comment
  pourrait-il le faire ? En effet, les opérations effectuées, telles
  que les différents appels à la fonction « randint » doivent
  l’être. Dit autrement, on ne peut optimiser le hasard ! Cependant,
  il est possible d’aller plus loin en calculant une complexité
  algorithmique. Celle-ci peut être vue comme le nombre d’appels à la
  fonction « randint » du programme. Dans le cadre d’une décroissance
  exponentielle (cas d’un nombre de dés supérieur ou égal à 1000), ce
  nombre vaut :


  Avec  et .


  On peut ensuite vérifier que si le nombre de dés est multiplié par
  10, alors la durée d’exécution de la liste sera aussi multipliée
  par 10. L’emploi de la fonction « time.time » du module « time »
  permet de telles vérifications (voir partie de programme ci-dessous)


Dés à $p$ faces
------------------

----
**todo** 
----

Comment modifier le programme précédent, si les dés ne sont plus
cubiques mais présentent chacun un nombre p entier de faces ? Il
suffit d’ajouter la variable p à toutes les fonctions définies du
programme. A titre d’exemple, la nouvelle programmation de la fonction
« lancerDés » est :


CONCLUSION
==========

Au lycée, il est possible de simuler la désintégration radioactive d’une collection d’atomes grâce au logiciel Pyzo permettant de programmer facilement avec le langage Python 3. Pour autant, la simulation ne doit pas faire oublier le modèle de dés, dans la mesure où celui-ci permet aux élèves de manipuler et de visualiser les différentes étapes qui devront par la suite être programmées par tous les élèves. De plus, travailler sur la désintégration radioactive a deux autres avantages : introduire les équations différentielles du premier ordre et être en synergie avec l’enseignement des probabilités de nos collègues de mathématiques.

REMERCIEMENT
============

Je remercie Léonard, étudiant en classes préparatoires, pour les différents échanges très pertinents que nous avons eus autour de la programmation en langage python 3.

BIBLIOGRAPHIE
=============

M. JACHYM, Cours de programmation avec le langage Python, Licence professionnelle, Métrologie dimensionnelle et qualité, IUT de St Denis, Université Paris 13
http://webserv.lurpa.ens-cachan.fr/~jachym/Cours-Python-part2.pdf



